<?php

namespace Drupal\message_notify_logger;

use Drupal\message\MessageInterface;
use Drupal\message_notify\MessageNotifier;
use Drupal\message_notify_logger\Entity\MessageNotifyLog;

class MessageNotifierWithLogging extends MessageNotifier {

  public function send(MessageInterface $message, array $options = [], $notifier_name = 'email') {
    $result = parent::send($message, $options, $notifier_name);
    MessageNotifyLog::create([
      'type' => $message->bundle(),
      'message_id' => $message->id(),
      'result' => $result,
      'uid' => $message->getOwnerId(),
      'timestamp' => \Drupal::time()->getCurrentTime(),
    ])->save();
  }



}
