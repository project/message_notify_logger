<?php

namespace Drupal\message_notify_logger;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the message_notify.sender service.
 */
class MessageNotifierLoggerServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Note: it's safest to use hasDefinition() first, because getDefinition() will
    // throw an exception if the given service doesn't exist.
    if ($container->hasDefinition('message_notify.sender')) {
      $definition = $container->getDefinition('message_notify.sender');
      $definition->setClass('Drupal\message_notify_logger\MessageNotifierWithLogging');
    }
  }

}
