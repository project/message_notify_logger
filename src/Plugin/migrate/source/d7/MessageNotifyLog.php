<?php

namespace Drupal\message_notify_logger\Plugin\migrate\source\d7;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 message notify log source from database.
 *
 * @MigrateSource(
 *   id = "d7_message_notify_log",
 *   source_module = "message_notify_logger",
 * )
 */
class MessageNotifyLog extends DrupalSqlBase {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('message_notify_log', 'message_notify_log')
      ->fields('message_notify_log')
      ->distinct()
      ->orderBy('id');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('The message log ID.'),
      'type' => $this->t('The message type sent.'),
      'message_id' => $this->t('The ID of the message'),
      'uid' => $this->t('The UID that the message was sent to.'),
      'result' => $this->t('The result of sending the message.'),
      'timestamp' => $this->t('The timestamp of when the message was sent.'),
    ];
    return $fields;
  }

}
