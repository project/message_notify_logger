<?php

namespace Drupal\message_notify_logger;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a message notify log entity type.
 */
interface MessageNotifyLogInterface extends ContentEntityInterface {

}
